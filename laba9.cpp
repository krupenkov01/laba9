#include <iostream>
#include "filereader.h"
#include "struct.h"
#include "work.h"

using namespace std;

int main()
{
	setlocale(LC_ALL, "Rus");
	Data data[5];
	reader(data, 5);
	cout << "Выберите действие:" << endl << "0 - Вывести протокол использования сети Интернет программой Skype" << endl << "1 - Вывести протокол использования сети Интернет после 8:00:00" << endl << endl;
	int a;
	cin >> a;
	if (a == 0) {
		show_skype(data, 5);
	}
	else if (a == 1) {
		show_time(data, 5);
	}
	else {
		cout << "Введено недопустмое значение.";
		return 0;
	}

	int b, c;
	cout << endl << "Выберите вид сортировки:" << endl << "0 - Сортировка вставками (Insertion sort)" << endl << "1 - Быстрая сортировка (Quick sort)" << endl << endl;
	cin >> b;
	cout << endl << "Выберите вид компоратора:" << endl << "0 - По убыванию времени использования сети Интернет" << endl << "1 - По возрастанию названия программы" << endl << endl;
	cin >> c;
	if (a == 0) {
		insert_sort(data, 5, 0, comp[c]);
	}
	else if (a == 1) {
		quickSort(data, 0, 5, comp[c]);
	}
	else {
		cout << "Введено недопустмое значение.";
		return 0;
	}
	for (int i = 0; i < 5; i++) {
		cout << data[i].begin << " " << data[i].end << " " << data[i].post << " " << data[i].prog << endl;
	}
}
