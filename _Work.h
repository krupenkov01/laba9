#ifndef WORK
#define WORK
#include <string>
#include <iostream>
#include "struct.h"
#include "filereader.h"
#include <array>


void show_skype(Data* data, int n) {
    for (int i = 0; i < n; i++) {
        if (data[i].prog.find("Skype") < 10) {
            cout << data[i].begin << " " << data[i].end << " " << data[i].post << endl;
        }
    }
}

void show_time(Data* data, int n) {
    for (int i = 0; i < n; i++) {
        if (data[i].end > "08:00:00") {
            cout << data[i].begin << " " << data[i].end << " " << data[i].post << endl;
        }
    }
}

void insert_sort(Data* data, int n, int s, bool comp(Data a, Data b))
{
    for (int i = 1; i < n; i++)
        for (int j = i; j > 0 && comp(data[j - 1], data[j]); j--)
            swap(data[j - 1], data[j]);

}
void quickSort(Data* array, int low, int high, bool comp(Data, Data))
{
    int i = low;
    int j = high;
    Data pivot = array[(i + j) / 2];

    while (i <= j)
    {
        while (comp(array[i], pivot))
            i++;
        while (!comp(array[j], pivot))
            j--;
        if (i <= j)
        {
            Data temp = array[i];
            array[i] = array[j];
            array[j] = temp;
            i++;
            j--;
        }
    }
    if (j > low)
        quickSort(array, low, j, comp);
    if (i < high)
        quickSort(array, i, high, comp);
}

bool comp1(Data a, Data b) {
    return a.end < b.end;
}

bool comp2(Data a, Data b) {
    if (a.prog == b.prog) {
        return(a.get + a.post) < (b.get + b.post);
    }
    else {
        return a.prog > b.prog;
    }
    // comp1 � comp2 - �����������
}

bool (*comp[])(Data, Data) = { *comp1, *comp2 };

#endif