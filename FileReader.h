#ifndef READER
#define READER
#include <string>
#include <fstream>
#include "struct.h"

using namespace std;

ifstream in("data.txt");

void reader(Data* data, int n) {
	for (int i = 0; i < n; i++) {
		in >> data[i].begin >> data[i].end >> data[i].get >> data[i].post >> data[i].prog;
	}
}

#endif