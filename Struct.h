#ifndef STRUCT
#define STRUCT
#include <string>

struct Data {
	std::string begin;
	std::string end;
	double get;
	double post;
	std::string prog;
};

#endif